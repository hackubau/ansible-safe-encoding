#!/usr/bin/python

#Author: marco guassone - marco.guassone@gmail.com

# Copyright: (c) 2018, Terry Jones <marco.guassone@gmail.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: safe_encoding

short_description: Modulo di encoding per le stringhe di python

version_added: "2.4"

description:
    - "Questo"

options:
    name:
        description:
            - This is the message to send to the test module
        required: true
    new:
        description:
            - Control to demo if the result of this module is changed or not
        required: false

extends_documentation_fragment:
    - azure

author:
    - Marco Guassone (marco.guassone@gmail.com)
'''

EXAMPLES = '''
# remove all non ascii characters
- name: Test with a message
  safe_encoding:
    string: hello world

# remove all non ascii characters all apex and slashes plus the letter 'A' and symbol '?'
- name: Test with a message
  safe_encoding:
    string: hello world
    removeApex: true
    removeSlashes: true
    clean:
      - 'A'
      - '?'

'''

RETURN = '''
original_message:
    description: The original name param that was passed in
    type: str
    returned: always
message:
    description: The output message that the test module generates
    type: str
    returned: always
'''

from ansible.module_utils.basic import AnsibleModule
#from unidecode import unidecode
import sys,codecs


def remove_non_ascii(text):
    return unidecode(unicode(text, encoding = "utf-8"))

def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        string=dict(type='str', required=True),
        removeApex=dict(type='bool', required=False, default=False),
        removeSlashes=dict(type='bool', required=False, default=False),
        truncate=dict(type='int', required=False, default=0),
        clean=dict(type='list',required=False,default=[])
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        original_message='',
        message=''
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)

    # manipulate or modify the state as needed (this is going to be the
    # part where your module will do what it needs to do)
    cioppibau=module.params['string']
    result['original_message'] = cioppibau
    #encoded=remove_non_ascii(cioppibau)
    if module.params['removeApex']:
        cioppibau=cioppibau.replace('"','')
        cioppibau=cioppibau.replace("'",'')
    if module.params['removeSlashes']:
        cioppibau=cioppibau.replace("\\",'')
        cioppibau=cioppibau.replace("/",'')
    for cl in module.params['clean']:
        cioppibau=cioppibau.replace(cl,'')
    if module.params['truncate']>0:
        cioppibau=cioppibau[0:module.params['truncate']]
    if module.params['truncate']<0:
        cioppibau=cioppibau[module.params['truncate']:]


    encoded = cioppibau.encode('ascii',errors='ignore')

    result['message'] = encoded
    result['stdout'] = encoded

    # use whatever logic you need to determine whether or not this module
    # made any modifications to your target
    if module.params['string']:
        result['changed'] = True

    # during the execution of the module, if there is an exception or a
    # conditional state that effectively causes a failure, run
    # AnsibleModule.fail_json() to pass in the message and the result
    if module.params['string'] == 'failed':
        module.fail_json(msg='You requested this to fail', **result)

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)

def getSessionId(ipAddress, username, password):
    # Gets the session ID to host 172.31.31.121 (Student 12)
    #c = httplib.HTTPSConnection(""172.31.31.121"")
    c = httplib.HTTPSConnection(ipAddress)
    #c.request(""GET"", ""/services/rest/V2/?method=authenticate&username=admin&password=a10&format=json"")
    c.request("GET", "/services/rest/V2/?method=authenticate&username="+username+"&"+password+"=a10&format=json")
    response = c.getresponse()
    data = json.loads(response.read())
    session_id = data['session_id']
    return session_id

def main():
    run_module()

if __name__ == '__main__':
    main()